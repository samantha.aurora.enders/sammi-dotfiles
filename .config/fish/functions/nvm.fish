#!/usr/bin/env fish

# taken from the example in https://github.com/edc/bass
function nvm
    bass source $HOME/.nvm/nvm.sh --no-use ';' nvm $argv
end