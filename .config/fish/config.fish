#!/usr/bin/env fish

# ripped from
# https://github.com/jonhoo/configs/blob/master/shell/.config/fish/config.fish

abbr -a e nvim
abbr -a n nvim
abbr -a g git
abbr -a gah 'git stash; and git pull --rebase; and git stash pop'

if status --is-interactive
    tmux ^ /dev/null; and exec true
end

if command -v exa > /dev/null
    abbr -a l 'exa'
    abbr -a ls 'exa'
    abbr -a ll 'exa -l'
    abbr -a la 'exa -la'
    abbr -a lh 'exa -lh'
else
    abbr -a l 'ls'
    abbr -a ll 'ls -l'
    abbr -a la 'ls -la'
    abbr -a lh 'ls -lh'
end

function fish_greeting
    if command -v neofetch > /dev/null
        neofetch
    end
end

# git aliases

git config --global alias.cp 'cherry-pick'
git config --global alias.st 'status -sb'
git config --global alias.cl 'clone'
git config --global alias.ci 'commit'
git config --global alias.co 'checkout'
git config --global alias.br 'branch'
git config --global alias.dc 'diff --cached'
git config --global alias.lg "log --graph --pretty=format:'%C(magenta)%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(cyan)<%an>%Creset' --abbrev-commit --date=relative --all"
git config --global alias.last 'log -1 --stat'
git config --global alias.unstage 'reset HEAD --'
git config --global alias.up 'push'
git config --global alias.aliases 'config --global --get-regexp alias'

# autojump
if test -f /usr/local/share/autojump/autojump.fish
    source /usr/local/share/autojump/autojump.fish
end

if test -f /usr/share/autojump/autojump.fish
    source /usr/share/autojump/autojump.fish
end

# rust
if test -d $HOME/.cargo
    source $HOME/.cargo/env
end

# fnm
set PATH $HOME/.fnm $PATH
fnm env --multi | source
